from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView

from todos.models import TodoList, TodoItem
# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    tempelate_name = "todos/list.html"

class TodoItemListView(ListView):
    model = TodoItem
    tempelate_name = "todos/list.html"

